
public class WeltDerZahlen { 
	
	public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten = 9;
    
    // Anzahl der Sterne in unserer Milchstra�e
    
    long anzahlSterne = 200000000000l;
    
    // Wie viele Einwohner hat Berlin?
    int bewohnerBerlin = 3769000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
    int alterTage = 7252;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
    int gewichtKilogramm = 190000;  
    
    // Schreiben Sie auf, wie viele km� das gr��te Land er Erde hat?
    int flaecheGroessteLand = 17098242;
    
    // Wie gro� ist das kleinste Land der Erde?
    
    double flaecheKleinsteLand = 0.44;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
    
    System.out.println("Anzahl der Sterne in der Milchstra�e: " + anzahlSterne);
    
    System.out.println("Anzahl der Bewohner Berlins: " + bewohnerBerlin);
    
    System.out.println("Anzahl der Tage die ich lebe: " + alterTage);
    
    System.out.println("Gewicht in Kilogramm des schwersten Tieres auf der Erde: " + gewichtKilogramm);
    
    System.out.println("Fl�che in km� des gr��tes Landes der Erde: " + flaecheGroessteLand);
    
    System.out.println("Fl�che in km� des kleinsten Landes der Erde: " + flaecheKleinsteLand);
    
    
    
    System.out.println("\n *******  Ende des Programms  ******* ");
    
  }
}